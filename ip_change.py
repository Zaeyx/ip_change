#!/usr/bin/env python
"""ip_change.py monitors ip, alerts you if it changes"""

import sys
import time
from gi.repository import Notify
from subprocess import check_output

#Init variables
ip = ''
if len(sys.argv) > 1:
	interface = str(sys.argv[1])
else:
	interface = "eth0"

def alert(old, new):
	Notify.init ("ip_change")
	Alert=Notify.Notification.new("Interface: " + interface + " has changed IP", old + " >> " + new, "dialog=information")
	Alert.show()

def get_ip():
	output = check_output(["hostname -I", interface], shell=True)
	try:
		out = output.split()[0]
	except:
		out = "Unassigned"
	return out

ip = get_ip()


if __name__ == "__main__":
	while True:
		temp_ip = get_ip()
		if ip != temp_ip:
			alert(ip, temp_ip)
			ip = temp_ip
		time.sleep(30)